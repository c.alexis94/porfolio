<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8"/>
		<link href="https://fonts.googleapis.com/css?family=Raleway:500,700" rel="stylesheet">
		<link rel="stylesheet" href="../css/bootstrap/bootstrap.min.css"/>
		<link rel="stylesheet" href="../css/lestyle.css"/>
		<title> COURNAL Alexis </title>
	</head>

	<body>	

		<header class="container-fluid header">
			<div class="container">
				<article class="">
					<h1>COURNAL Alexis</h1>
						<nav class="list-inline">					
							<a href ="../index.html">ACCUEIL</a>
							<a href ="cv.html">CV</a>
							<!--<a href ="referentiel.php">REFERENTIEL</a>-->
							<a href ="ppe.html">PPE</a>
							<a href ="projets.html">PROJETS</a>
							<a href ="stages.html">STAGES</a>
							<!--<a href ="tableau.html">TABLEAU DE SYNTHESE</a>-->
							<a href ="veille.html">VEILLE TECH. & JUR.</a>
							<a href ="contact.php">CONTACT</a>
						</nav>
				</article>	
			</div>
		</header>

		<section class="container-fluid projet">
			<div class="container">
				<h1>MON PROJET</h1>
				<article class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
					<p>Prochainement titulaire du BTS SIO (Services Informatiques aux Organisations), j'ai obtenu l'accord du Lycée Le Rebours à Paris afin d'intégrer à la prochaine rentrée la Licence informatique STS (Sciences, Technologies, Santé) Développement Web & Logiciel en alternance.</p>

					<p>Cet apprentissage en alternance pourra ainsi me permettre d'acquérir les savoirs pratiques nécessaires à l'obtention de mon diplôme et à la poursuite de mes études. Je souhaiterais en effet par la suite préparer un Master informatique par la voie de l'alternance et concrétiser de ce fait mon projet professionnel en devenant Développeur informatique.</p>
				</article>
			</div>
		</section>

		<section class="container fluid referentiel">
			<div class="container">
				<div class="ref">
					<h1>RÉFÉRENTIEL BTS SIO</h1>
					<img src="../ImageEtPdf/projet.jpg" alt="projet"/>
					<article class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
						<h2>Détails du programme :</h2><br>
						<p>Les interventions sont différentes selon les processus auxquels il participe :</p>
						<p><ul>
								<li>Il intervient dans les processus « Production de services », « Fourniture de services » et « Gestion du patrimoine informatique » sur les éléments des solutions techniques liées à son domaine d’expertise et sur la base de compétences professionnelles communes aux deux option.</li><br>
								<li>Il intervient dans le processus lié à son domaine de spécialité :</li>
							</ul></p>

						<ol>
							<li>Conception et maintenance de solutions d’infrastructure</li> 
							<li>Conception et maintenance de solutions applicatives</li>
						</ol>
						</p><br><br>

						<h2>Tableau des matières :</h2><br>
						<ol>
							<li>CULTURE GÉNÉRALE ET EXPRESSION</li>
							<li>EXPRESSION ET COMMUNICATION EN LANGUE ANGLAISE</li>
							<li>MATHÉMATIQUES POUR L’INFORMATIQUE</li>
							<li>MATHÉMATIQUES APPROFONDIES</li>
							<li>ALGORITHMIQUE APPLIQUÉE</li>
							<li>ANALYSE ÉCONOMIQUE, MANAGÉRIALE ET JURIDIQUE DES SERVICES INFORMATIQUES</li>
							<li>SOLUTIONS INFORMATIQUES (SI) :
								<ul>
									<li>SI1 - Support système des accès utilisateurs</li>
									<li>SI2 - Support réseau des accès utilisateurs</li>
									<li>SI3 - Exploitation des données</li>
									<li>SI4 – Base de la programmation</li>
									<li>SI5 - Support des services et des serveurs</li>
									<li>SI6 - Développement d’applications</li>
									<li>SI7 - Intégration et adaptation d’un service</li>
								</ul>
							</li>
							<li>SOLUTIONS LOGICIELLES ET APPLICATIONS MÉTIERS (SLAM) :
								<ul>
									<li>SLAM1 - Exploitation d’un schéma de données</li>
									<li>SLAM2 - Programmation objet</li>
									<li>SLAM3 - Conception et adaptation d’une base de données</li>
									<li>SLAM4 - Réalisation et maintenance de composants logiciels</li>
									<li>SLAM5 – Conception et adaptation de solutions applicatives</li>
								</ul>
							</li>
						</ol>
						<br><br>

						<h2>A propos du stage</h2><br>
						<p>Les stages sont destinés à donner à l’étudiant une représentation concrète du milieu professionnel des services informatiques et de l’emploi, tout en lui permettant d’acquérir et d’éprouver les compétences professionnelles prévues par le référentiel. Ils contribuent au développement de son expérience professionnelle et lui permettent d’alimenter son portefeuille de compétences professionnelles à partir des situations réelles vécues ou observées et de conserver ainsi des traces pertinentes des observations, analyses et travaux réalisés dans ce cadre.Ils constituent des supports privilégiés pour :
						se situer dans un environnement organisationnel réel et s’immerger dans des contextes professionnels variés ;
						construire une représentation des métiers d’un prestataire informatique dans toutes leurs dimensions : production et fourniture de services, conception et maintenance de solutions techniques, relations avec les parties prenantes, conseil et assistance aux utilisateurs, veille technologique, etc. ;
						acquérir et développer des attitudes et des comportements professionnels adaptés, en prenant en compte les contraintes s’exerçant dans chacune des activités réalisées.</p>
					</article>
				</div>
			</div>
	</section>

	<footer class="container-fluid footer">
			©Copyright 2019 - Tous droits réservés
	</footer>

	</body>
</html>
