<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
  <head>
    <meta charset="utf-8"/>
    <link href="https://fonts.googleapis.com/css?family=Raleway:500,700" rel="stylesheet"/>
    <link rel="stylesheet" href="../css/bootstrap/bootstrap.min.css"/>
    <link rel="stylesheet" href="../css/lestyle.css"/>
    <title> COURNAL Alexis </title>
  </head>

<body>

<header class="container-fluid header">
      <div class="container">
        <article class="">
          <h1>COURNAL Alexis</h1>
            <nav class="list-inline">         
              <a href ="../index.html">ACCUEIL</a>
              <a href ="cv.html">CV</a>
							<a href ="ETNA-BROC-ALTERNANCE-2021-2022.pdf#page=16" target="_blank">ETNA</a>
							<a href ="ppe.html">PPE - BTS</a>
              <a href ="projets.html">PROJETS</a>
              <a href ="stages.html">STAGES</a>
              <!--<a href ="tableau.html">TABLEAU DE SYNTHESE</a>-->
              <a href ="veille.html">VEILLE TECH. & JUR.</a>
              <a href ="contact.php">CONTACT</a>
            </nav>
        </article>  
      </div>
    </header>

<?php
// S'il y des données de postées
if ($_SERVER['REQUEST_METHOD']=='POST') 
{
  // Code PHP pour traiter l'envoi de l'email
  
  $nombreErreur = 0; // Variable qui compte le nombre d'erreur
  
  // Définit toutes les erreurs possibles
  if (!isset($_POST['nom'])) 
  {
    $nombreErreur++;
    $erreur1 = '<p>Il y a un problème avec la variable "nom".</p>';
  }

  else 
  {
    if (empty($_POST['nom'])) 
    {
      $nombreErreur++;
      $erreur2 = '<p>Vous n\'avez pas renseigné votre nom et prénom</p>';
    }
  }

  if (!isset($_POST['email'])) 
  { // Si la variable "email" du formulaire n'existe pas (il y a un problème)
    $nombreErreur++; // On incrémente la variable qui compte les erreurs
    $erreur3 = '<p>Il y a un problème avec la variable "email".</p>';
  } 

  else 
  { // Sinon, cela signifie que la variable existe (c'est normal)

    if (empty($_POST['email'])) 
    { // Si la variable est vide
      $nombreErreur++; // On incrémente la variable qui compte les erreurs
      $erreur4 = '<p>Vous n\'avez pas renseigné votre email'.'</p>';
    } 

    else 
    {
      if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) 
      {
        $nombreErreur++; // On incrémente la variable qui compte les erreurs
        $erreur5 = '<p>Cet email ne ressemble pas à un email</p>';
      }
    }
  }
  
  if (!isset($_POST['message'])) 
  {
    $nombreErreur++;
    $erreur6 = '<p>Il y a un problème avec la variable "message".</p>';
  }

  else 
  {
    if (empty($_POST['message'])) 
    {
      $nombreErreur++;
      $erreur7 = '<p>Vous n\'avez pas renseigné votre message</p>';
    }
  }
  
  if (!isset($_POST['captcha'])) 
  {
    $nombreErreur++;
    $erreur8 = '<p>Il y a un problème avec la variable "captcha"</p>';
  } 

  else 
  {
    if ($_POST['captcha']!=4) 
    {
      $nombreErreur++;
      $erreur9 = '<p>Désolé, le captcha anti-spam est erroné</p>';
    }
  }

  if ($nombreErreur==0) { // S'il n'y a pas d'erreur
    // Récupération des variables et sécurisation des données
    $nom = htmlentities($_POST['nom']); // htmlentities() convertit des caractères "spéciaux" en équivalent HTML
    $email = htmlentities($_POST['email']);
    $message = htmlentities($_POST['message']);
    
    // Variables concernant l'email
    $destinataire = 'alexis.cournal@outlook.com'; // Adresse email du webmaster
    $sujet = 'Mail portefolio'; // Titre de l'email
    $contenu = '<html><head><title>Titre du message</title></head><body>';
    $contenu .= '<p>Bonjour, vous avez reçu un message à partir de votre site web.</p>';
    $contenu .= '<p><strong>Nom</strong>: '.$nom.'</p>';
    $contenu .= '<p><strong>Email</strong>: '.$email.'</p>';
    $contenu .= '<p><strong>Message</strong>: '.$message.'</p>';
    $contenu .= '</body></html>'; // Contenu du message de l'email
    
    // Pour envoyer un email HTML, l'en-tête Content-type doit être défini
    $headers = 'MIME-Version: 1.0'."\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1'."\r\n";
    
    @mail($destinataire, $sujet, $contenu, $headers); // Fonction principale qui envoi l'email
    
    echo '<br><h2>Message envoyé !</h2>'; // Afficher un message pour indiquer que le message a été envoyé
  } 

  else 
  { // S'il y a un moins une erreur
    echo '<div style="padding-top:110px; padding-left: 15px">';
    echo '<p style="color:#ff0000;">Désolé, il y a eu '.$nombreErreur.' erreur(s). Voici le détail des erreurs:</p>';
    if (isset($erreur1)) echo '<p>'.$erreur1.'</p>';
    if (isset($erreur2)) echo '<p>'.$erreur2.'</p>';
    if (isset($erreur3)) echo '<p>'.$erreur3.'</p>';
    if (isset($erreur4)) echo '<p>'.$erreur4.'</p>';
    if (isset($erreur5)) echo '<p>'.$erreur5.'</p>';
  	if (isset($erreur6)) echo '<p>'.$erreur6.'</p>';
  	if (isset($erreur7)) echo '<p>'.$erreur7.'</p>';
    if (isset($erreur8)) echo '<p>'.$erreur8.'</p>';
    if (isset($erreur9)) echo '<p>'.$erreur9.'</p>';
    echo '</div>';
  }
}
?>

<section class="container-fluid formcontact">
      <h1>CONTACT</h1><br><br>

      <form id="contact" method="post" action="contact.php" class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
        <div class="row">
                <fieldset>

                  <!--<p>Téléphone :
                  <span class="grey">07-50-94-11-29</span></p>
         
                  <p>Email :
                  <span class="grey">c.alexis94@gmail.com</span></p>-->

                   <input type="text" id="nom" name="nom" class="input-block-level"  placeholder="Nom et Prénom"><br><br>

                    <input type="text" id="email" name="email" class="input-block-level" placeholder="Email"><br><br>

                    <textarea rows="3" id="message" name="message" class="input-block-level" placeholder="Message"></textarea><br><br>

                    <p><span style="color:#ff0000;"></span> <input type="text" name="captcha" size="2" placeholder="Combien font 1+3" ></p>

                    <button type="submit" class="btn btn-custom" name="submit" value="Envoyer">Envoyer</button>
                </fieldset>
        </div>
      </form>
    </section> 

    <footer class="container-fluid footercontact">
      ©Copyright 2021 - Tous droits réservés
    </footer>

  </body>
</html>