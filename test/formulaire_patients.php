<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="medStyle.css"/>
            <link href="https://fonts.googleapis.com/css?family=Raleway:500,700" rel="stylesheet">
            <meta name="viewport" content="width=device-width" />
            <!--<link rel="stylesheet" href="../css/bootstrap/bootstrap.min.css"/>-->
        <title>Formulaire 1ère consultation</title>
    </head>

    <body>
    <div class="container-fluid">
        <nav class="navbar navbar-light bg-light">
            <div class="container-fluid">
                <div style="margin-right : auto">      
                    <h2 style="color: #5356D1"><i>StockinDoc</i></h2>
                </div>
            </div>
        </nav>
    </div>
        <div id="contenu" height="auto">
            <form id="formulaire" action="formulaire_patients.php" method="post" class="form">
                <h1 style="font-size: 40px">Questionnaire - Patient</h1> 
                <hr>
                <div class="form">
                    <label>Votre prénom :</label>
                    <input type="text" name="prenom" id="prenom" value="" required/>
                </div>
                <div class="form">
                    <label>Votre nom :</label>
                    <input type="text" name="nom" value="" required/>
                </div>
                <div class="form">
                    <label>Votre date de naissance :</label>
                    <input type="date" name="date_naiss" value="" required/>
                </div>
                <div class="form">
                    <label>Votre adresse :</label>
                    <input type="text" name="adresse" value="" required/>
                </div>
                <div class="form">
                    <label>Ville de résidence :</label>
                    <input type="text" name="ville" value="" required/>
                </div>
                <div class="form">
                    <label>Code postal :</label>
                    <input type="text" name="cp"  value="" required/>
                </div>
                <div class="form">
                    <label>Numéro de sécurité sociale :</label>
                    <input type="text" name="Num_secu" id="Num_secu" required/>
                </div>
                <div class="form">
                    <label>Vos antécédents médicaux :</label>
                    <textarea name="antecedent" value=""></textarea>
                </div><br>
                <div class="form">
                    <label>Votre poids :</label>
                    <input type="number" name="poids" placeholder="En kg" value="" required/>
                </div>
                <div class="form">
                    <label>Votre taille :</label>
                    <input type="number" name="taille" placeholder="En cm" value="" required/>
                </div>
                <div class="form">
                    <label>Votre groupe sanguin :</label>
                    <input type="text" name="groupe_sanguin" placeholder="A+, B+, O-..." value="" required/>
                </div>
                <div class="form">
                    <input id="submit" type="submit" name="valider" value="valider" />
                </div>
            </form>
        </div>
    </body>        

    <?php
    include 'cnx.php';

    if (isset($_POST['valider']))
    {
        

        $sql = $cnx->prepare("INSERT INTO medpat (prenom, nom, date_naiss, Adresse, ville, cp, Num_secu, poids, taille, groupe_sanguin, statut)
            VALUES ('".$_POST['prenom']."', '".$_POST['nom']."', '".$_POST['date_naiss']."', '".$_POST['adresse']."','".$_POST['ville']."', '".$_POST['cp']."', '".$_POST['Num_secu']."', '".$_POST['poids']."','".$_POST['taille']."', '".$_POST['groupe_sanguin']."', false)");
        $sql->execute();

        header("Location: deuxiemeForm.php");
    
    }
    ?>
</html>